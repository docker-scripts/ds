cmd_inject_help() {
    cat <<_EOF
    inject [<script>]
        Inject and run a script inside the container.

_EOF
}

cmd_inject() {
    local script=$1; shift
    if [[ -z $script ]]; then
        echo -e "Usage:\n$(cmd_inject_help)"
        _print_script_list
        return 0
    fi

    # get the absolute path of the script
    local scrfile
    [[ -x $LIBDIR/inject/$script ]] && scrfile=$LIBDIR/inject/$script
    [[ -x $APPDIR/inject/$script ]] && scrfile=$APPDIR/inject/$script
    [[ -x ./inject/$script ]] && scrfile=./inject/$script
    [[ -f $scrfile ]] || fail "\n--> Script '$script' not found.\n"
    [[ -x $scrfile ]] || fail "\n--> Script '$script' not executable.\n"

    # copy to a tmp dir and run from inside the container
    rm -rf tmp/
    mkdir tmp/
    cp $scrfile tmp/
    [[ -f $DSDIR/global_settings.sh ]] \
        && cp $DSDIR/global_settings.sh tmp/
    echo -e "\n--> Running script '$scrfile'"
    cmd_exec "/host/tmp/$(basename $scrfile)" "$@"
    local err=$?
    rm -rf tmp/
    return $err
}

_print_script_list() {
    # general scripts
    local script_list=""
    [[ -d $LIBDIR/inject/ ]] && script_list=$(ls $LIBDIR/inject/)
    script_list="$(echo $script_list | sed -e 's/ / ; /g')"
    [[ -n $script_list ]] \
        && echo -e "\nGeneral scripts:\n   " $script_list

    # specific scripts
    script_list=""
    [[ -d $APPDIR/inject/ ]] && script_list=$(ls $APPDIR/inject/)
    script_list="$(echo $script_list | sed -e 's/ / ; /g')"
    [[ -n $script_list ]] \
        && echo -e "\nSpecific scripts:\n   " $script_list

    # container scripts
    script_list=""
    [[ -d ./inject/ ]] && script_list=$(ls ./inject/)
    script_list="$(echo $script_list | sed -e 's/ / ; /g')"
    [[ -n $script_list ]] \
        && echo -e "\nContainer scripts:\n   " $script_list
}
