#!/bin/bash -x
### set a new prompt

source /host/settings.sh
echo $CONTAINER > /etc/debian_chroot

sed -e '/^PS1=/d' -i /root/.bashrc
cat << 'EOF' | tee -a /root/.bashrc
PS1='${debian_chroot:+($debian_chroot)}\[\033[01;36m\]\u\[\033[01;33m\]@\[\033[01;35m\]\h \[\033[01;33m\]\w \[\033[01;31m\]\$ \[\033[00m\]'
EOF
