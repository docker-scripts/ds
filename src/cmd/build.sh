cmd_build_help() {
    cat <<_EOF
    build
        Build the docker image.

_EOF
}

cmd_build() {
    # make available the settings as environment variables
    # so that they can be used my m4 inside the Dockerfile
    set -a
    [[ -f $DSDIR/global_settings.sh ]] \
        && source $DSDIR/global_settings.sh
    source settings.sh
    set +a

    # copy docker files to a tmp dir
    # and preprocess Dockerfile
    local tmp=$(mktemp -u)
    cp -aT $APPDIR $tmp
    m4 -I $APPDIR -I "$LIBDIR/dockerfiles" \
       $APPDIR/Dockerfile > $tmp/Dockerfile

    # build the image
    log docker build "$@" --tag=$IMAGE $tmp/

    # clean up
    rm -rf $tmp/
}
