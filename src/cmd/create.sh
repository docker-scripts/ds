cmd_create_help() {
    cat <<_EOF
    create
        Create the docker container.

_EOF
}

cmd_create() {
    # create a ds network if it does not yet exist
    local subnet=''
    [[ -n $SUBNET ]] && subnet="--subnet $SUBNET"
    docker network inspect $NETWORK &>/dev/null \
        || docker network create $subnet $NETWORK

    # remove the container if it exists
    cmd_stop
    docker network disconnect $NETWORK $CONTAINER 2>/dev/null
    docker rm $CONTAINER 2>/dev/null

    # create a new container
    docker create --name=$CONTAINER --hostname=$CONTAINER \
        --restart=unless-stopped \
        --mount type=bind,source=$(pwd),destination=/host \
        $(_systemd_options) \
        $(_published_ports) \
        $(_network_and_aliases) \
        "$@" $IMAGE
    docker start $CONTAINER

    # add DOMAIN to revproxy
    if [[ -n $DOMAIN ]]; then
        ds revproxy add
        ds revproxy ssl-cert
    fi
}

### Return the options needed for running systemd.
### See: https://github.com/mviereck/x11docker/issues/349#issuecomment-1034346442
_systemd_options() {
    local options='rw,rprivate,nosuid,nodev'
    echo "
        --cgroupns=host
        -v /sys/fs/cgroup:/sys/fs/cgroup
        --tmpfs /run
        --tmpfs /run/lock
        --tmpfs /tmp
        --tmpfs /var/log/journal
    "
}

### published ports
_published_ports() {
    [[ -n $PORTS ]] || return

    local ports=''
    for port in $PORTS; do
        ports+=" --publish $port"
    done

    echo "$ports"
}

### network and aliases
_network_and_aliases() {
    [[ -n $NETWORK ]] || return

    local network=" --network $NETWORK"
    network+=" --network-alias $CONTAINER"
    if [[ -n $DOMAIN ]]; then
        for domain in $DOMAIN $DOMAINS; do
            network+=" --network-alias $domain"
        done
    fi

    echo "$network"
}
