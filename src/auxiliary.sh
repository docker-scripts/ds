# Auxiliary functions.

fail() {
    echo -e "$@" >&2
    exit 1
}

# Change the name of an existing function.
# Useful when redefining standard commands.
rename_function() {
  test -n "$(declare -f $1)" &&
  eval "${_/$1/$2}" &&
  unset -f "$1";
}

# Test whether the given name is a function.
is_function() {
    declare -Ff "$1" >/dev/null
    return $?
}

# Check whether a container is up.
is_up() {
    local container=${1:-$CONTAINER}
    docker ps | grep $container >/dev/null
    return $?
}

# Run the given command and log its output.
log() {
    local datestamp=$(date +%F | tr -d -)
    local logfile=logs/$CONTAINER-$datestamp.out
    mkdir -p logs/
    cat << _EOF >> $logfile

################################################################################
###    Time: $(date)
### Command: $*
################################################################################

_EOF
    $* 2>&1 | tee -a $logfile
}

# Run docker with the given arguments.
docker() {
    [[ $SHOW_DOCKER_COMMANDS ]] && \
        echo "+ docker" "$@" | highlight -S bash -O xterm256 --style=navy >&2
    $(which docker) "$@"
}

# Highlight the lines that start with a "+"
highlight_output() {
    while read line; do
        case $line in
            +*)
                echo "$line" | highlight -S bash -O xterm256 --style=greenlcd >&2
                ;;
            *)
                echo "$line"
                ;;
        esac
    done
}

# Get the public ip of the host
get_public_ip() {
    local services='ifconfig.co ifconfig.me icanhazip.com'
    local pattern='^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$'
    local ip
    for service in $services; do
        ip=$(curl -s -4 $service)
        [[ $ip =~ $pattern ]] && echo $ip && return
    done
    return 1
}
