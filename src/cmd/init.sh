cmd_init_help() {
    cat <<_EOF
    init <app> [@<container>]
        Initialize a container directory by getting the file 'settings.sh'
        from the given app directory.

        The argument <app> can be a subdirectory of '$SCRIPTS'
        (where the docker-scripts are usually stored), or an absolute/relative
        path that contains the scripts of the app.

        If the second argument is missing, the current directory will be used
        for initializing the container. If <container> starts with './' or '../'
        it will be relative to the current directory. If <container> starts
        with '/', it will be an absolute path.
        Otherwise, the directory '$CONTAINERS/<container>' will be used.

_EOF
}

cmd_init() {
    # get app
    local app=$1
    [[ -n $app ]] || fail "Usage:\n$(cmd_init_help)"

    # check app dir
    local scripts_dir="$SCRIPTS/$app"
    [[ ${app:0:1} == '/' ]] && scripts_dir="$app"
    [[ -d "$scripts_dir" ]] || scripts_dir="$(realpath $app)"
    [[ -d "$scripts_dir" ]] || fail "Cannot find the script directory '$SCRIPTS/$app/' or '$scripts_dir/'."
    [[ "$scripts_dir" == "$SCRIPTS/$app" ]] || app="$scripts_dir"

    # check settings.sh
    [[ -f "$scripts_dir"/settings.sh ]] || fail "There is no file 'settings.sh' on '$scripts_dir'"

    # get container
    local container=$2
    if [[ -n $container ]]; then
        [[ "${container:0:1}" == '@' ]] || fail "Usage:\n$(cmd_init_help)"
        local dir="${container:1}"
        [[ "${dir:0:1}" == '/' || "${dir:0:2}" == './' || "${dir:0:3}" == '../' ]] || dir="$CONTAINERS/$dir"
        mkdir -p $dir
        cd $dir
        chmod o-rwx .
    fi

    # check for overriding
    [[ -f settings.sh ]] \
        && fail "File '$(pwd)/settings.sh' already exists.\nInitialization failed."

    # copy settings.sh
    cp "$scripts_dir"/settings.sh .
    chmod o-rwx settings.sh
    # make sure that APP contains the location of the scripts
    if grep -qe '^APP=' settings.sh; then
        sed -i settings.sh -e "/^APP=/ c APP=$app"
    else
        sed -i settings.sh -e "1i APP=$app"
    fi
    # add IMAGE, if missing
    if ! grep -qe '^IMAGE=' settings.sh; then
        local image=$(basename "$scripts_dir")
        sed -i settings.sh -e "/^APP=/ a IMAGE=$image"
    fi
    # add CONTAINER, if missing
    if ! grep -qe '^CONTAINER=' settings.sh; then
        local container=$(basename $(pwd))
        sed -i settings.sh -e "/^IMAGE=/ a CONTAINER=$container"
    fi

    # run any custom init steps for the app
    [[ -f "$scripts_dir"/misc/init.sh ]] && source "$scripts_dir"/misc/init.sh

    # notify
    echo "Container initialized on '$(pwd)/'."
}
